//! # rp-pico-lowpower-rust
//! Copyright (C) 2024 Seth Just
//!
//! This program is free software: you can redistribute it and/or modify
//! it under the terms of the GNU Affero General Public License as published
//! by the Free Software Foundation, either version 3 of the License, or
//! (at your option) any later version.
//!
//! This program is distributed in the hope that it will be useful,
//! but WITHOUT ANY WARRANTY; without even the implied warranty of
//! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//! GNU Affero General Public License for more details.
//!
//! You should have received a copy of the GNU Affero General Public License
//! along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![no_std]
#![no_main]

use core::cell::RefCell;

// Some short-cuts to useful types
use core::fmt::Write;
use core::ops::Add;
use core::time::Duration;

use critical_section::Mutex;

// For in the graphics drawing utilities like the font
// and the drawing routines
use embedded_graphics::{
    mono_font::{ascii::FONT_5X8, ascii::FONT_9X18_BOLD, MonoTextStyleBuilder},
    pixelcolor::BinaryColor,
    prelude::*,
    text::{Baseline, Text},
};
use embedded_graphics::mono_font::{MonoFont, MonoTextStyle};

// Some traits we need
use embedded_hal::digital::{OutputPin, StatefulOutputPin};
use fugit::RateExtU32;
use hal::{Clock, clocks::{ClockGate, ClocksManager, ClockSource}, gpio::{self, Pin}, i2c, pac, rtc};

// Our interrupt macro
use pac::interrupt;

// Ensure we halt the program on panic (if we don't mention this crate it won't
// be linked)
use panic_halt as _;

// Alias for our HAL crate
use rp2040_hal as hal;
use hal::gpio::Interrupt::EdgeLow;

// The RTC driver
use ds323x::{DateTimeAccess, Ds323x, NaiveDate, NaiveDateTime, Rtcc};
use ds323x::ic::DS3231;
use ds323x::interface::I2cInterface;
use rp2040_hal::rosc::RingOscillator;

// The OLED display driver
use ssd1306::{mode::BufferedGraphicsMode, prelude::*, Ssd1306};

// Pin types quickly become very long!
// We'll create some type aliases using `type` to help with that

/// This pin will be our output - it will drive an LED if you run this on a Pico
type LedPin = gpio::Pin<gpio::bank0::Gpio25, gpio::FunctionSioOutput, gpio::PullNone>;

/// This pin will be the source of our external clock interrupt
type IrqPin = gpio::Pin<gpio::bank0::Gpio15, gpio::FunctionSioInput, gpio::PullUp>;

type I2cDevice = i2c::I2C<pac::I2C1, (Pin<gpio::bank0::Gpio2, gpio::FunctionI2c, gpio::PullUp>, Pin<gpio::bank0::Gpio3, gpio::FunctionI2c, gpio::PullUp>)>;
type Oled = Ssd1306<I2CInterface<I2cDevice>, DisplaySize128x64, BufferedGraphicsMode<DisplaySize128x64>>;
type Rtc = Ds323x<I2cInterface<I2cDevice>, DS3231>;

/// Since we're always accessing the pin and the rtc together we'll store them in a tuple.
/// Giving this tuple a type alias means we won't need to use () when putting them
/// inside an Option. That will be easier to read.
type InitializedPeripherals = (LedPin, IrqPin, RingOscillator<hal::rosc::Enabled>, Rtc, FmtBuf);

/// This how we transfer our Led pin and RTC into the Interrupt Handler.
/// We'll have the option hold both using the LedAndRtc type.
/// This will make it a bit easier to unpack them later.
static GLOBAL_SHARED: Mutex<RefCell<Option<InitializedPeripherals>>> = Mutex::new(RefCell::new(None));

// TODO: make this dynamic based on estimations
static INTERVAL_SEC: u8 = 1;

#[rp_pico::entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks

    // // The default is to generate a 125 MHz system clock
    // let clocks = hal::clocks::init_clocks_and_plls(
    //     rp_pico::XOSC_CRYSTAL_FREQ,
    //     pac.XOSC,
    //     pac.CLOCKS,
    //     pac.PLL_SYS,
    //     pac.PLL_USB,
    //     &mut pac.RESETS,
    //     &mut watchdog,
    // )
    // .unwrap();

    // Configure custom clocks to save power
    let xosc_crystal_freq = rp_pico::XOSC_CRYSTAL_FREQ;
    let xosc_dev = pac.XOSC;
    let clocks_dev = pac.CLOCKS;
    let pll_sys_dev = pac.PLL_SYS;
    let pll_usb_dev = pac.PLL_USB;
    let resets = &mut pac.RESETS;

    // Below copied from init_clocks_and_plls, with small changes

    let rosc = RingOscillator::new(pac.ROSC).initialize();

    // Set up XOSC for accurate timekeeping
    // let xosc = hal::xosc::setup_xosc_blocking(xosc_dev, xosc_crystal_freq.Hz()).map_err(hal::clocks::InitError::XoscErr).unwrap();

    let mut clocks = {
        // Configure watchdog tick generation to tick over every microsecond
        watchdog.enable_tick_generation((xosc_crystal_freq / 1_000_000) as u8);

        let mut clocks = ClocksManager::new(clocks_dev);

        // Skip PLL setup to save power

        // let pll_sys = hal::pll::setup_pll_blocking(
        //     pll_sys_dev,
        //     xosc.operating_frequency(),
        //     hal::pll::common_configs::PLL_SYS_125MHZ
        //     &mut clocks,
        //     resets,
        // )
        //     .map_err(hal::clocks::InitError::PllError).unwrap();
        // let pll_usb = hal::pll::setup_pll_blocking(
        //     pll_usb_dev,
        //     xosc.operating_frequency(),
        //     hal::pll::common_configs::PLL_USB_48MHZ,
        //     &mut clocks,
        //     resets,
        // )
        //     .map_err(hal::clocks::InitError::PllError).unwrap();

        // clocks
        //     .init_default(&xosc, &pll_sys, &pll_usb)
        //     .map_err(hal::clocks::InitError::ClockError).unwrap();

        // Below copied from init_default, with small changes

        // Set system clock to CLK_REF...
        clocks.system_clock
            .configure_clock(&clocks.reference_clock, clocks.reference_clock.get_freq()).unwrap();

        // ...then set CLK_REF to run on ROSC
        clocks.reference_clock
            .configure_clock(&rosc, rosc.get_freq()).unwrap();

        // // ...then set CLK_REF to run on XOSC
        // // CLK_REF = XOSC (12MHz) / 1 = 12MHz
        // clocks.reference_clock
        //     .configure_clock(&xosc, xosc.get_freq()).unwrap();

        //// CLK USB = PLL USB (48MHz) / 1 = 48MHz
        //clocks.usb_clock
        //    .configure_clock(&pll_usb, pll_usb.get_freq()).unwrap();

        //// CLK ADC = PLL USB (48MHZ) / 1 = 48MHz
        //clocks.adc_clock
        //    .configure_clock(&pll_usb, pll_usb.get_freq()).unwrap();

        // // Lock the RTC to the crystal oscillator for accurate timekeeping
        // // even if other clocks (e.g. ref/sys/pll) sleep.
        // // CLK RTC = XOSC (12MHz) / 256 = 46875Hz
        // clocks.rtc_clock.configure_clock(&xosc, 46875.Hz()).unwrap();

        // CLK PERI = clk_sys. Used as reference clock for Peripherals. No dividers so just select and enable
        // Normally choose clk_sys or clk_usb
        clocks.peripheral_clock
            .configure_clock(&clocks.system_clock, clocks.system_clock.freq()).unwrap();

        clocks
    };

    // Configure the sleep enable register for low power between interrupts
    // Mask = 0 -- no clocks enabled
    let mut sleep_gate: hal::clocks::ClockGate = ClockGate(0);
    clocks.configure_sleep_enable(sleep_gate);

    // The single-cycle I/O block controls our GPIO pins
    let sio = hal::Sio::new(pac.SIO);

    // Set the pins to their default state
    let pins = gpio::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // Configure GPIO 25 as an output to drive our LED.
    // we can use reconfigure() instead of into_pull_up_input()
    // since the variable we're pushing it into has that type
    let mut led = pins.gpio25.reconfigure();
    let _ = led.set_high();

    let in_pin: IrqPin = pins.gpio15.reconfigure();

    // Set up  I²C peripheral
    let i2c = hal::I2C::i2c1(
       pac.I2C1,
       pins.gpio2.into_function::<hal::gpio::FunctionI2C>().into_pull_type(),
       pins.gpio3.into_function::<hal::gpio::FunctionI2C>().into_pull_type(),
       400.kHz(),
       &mut pac.RESETS,
       &clocks.peripheral_clock,
    );

    // // Create the I²C display interface
    // let interface = ssd1306::I2CDisplayInterface::new(i2c);
    //
    // // Initialize the display driver
    // let mut display = Ssd1306::new(interface, DisplaySize128x64, DisplayRotation::Rotate0)
    //    .into_buffered_graphics_mode();
    // display.init().unwrap();
    //
    // // Clear the display
    // let _ = display.clear();
    // let _ = display.flush();

    // Create the RTC driver
    let mut rtc = Ds323x::new_ds3231(i2c);

    if !rtc.running().unwrap() {
        rtc.set_datetime(&NaiveDateTime::default()).unwrap();
        rtc.enable().unwrap();
    }

    // Set up an initial alarm, then configure interrupts
    let now = rtc.time().unwrap();
    rtc.set_alarm1_hms(now.add(Duration::from_secs(1))).unwrap();
    rtc.use_int_sqw_output_as_interrupt().unwrap();
    rtc.enable_alarm1_interrupts().unwrap();

    in_pin.set_dormant_wake_enabled(EdgeLow, true);
    in_pin.set_interrupt_enabled(EdgeLow, true);

    // Give away our set-up objects by moving them into the `GLOBAL_SHARED` variable.
    // We won't need to access them in the main thread again
    critical_section::with(|cs| {
        GLOBAL_SHARED.borrow(cs).replace(Some((led, in_pin, rosc, rtc, FmtBuf::new())));
    });

    // Enable deep sleep
    pac.PPB.scr().write(|scr| { scr.sleepdeep().set_bit() });

    // Unmask the RTC IRQ so that the NVIC interrupt controller
    // will jump to the interrupt function when the interrupt occurs.
    // We do this last so that the interrupt can't go off while
    // it is in the middle of being configured
    unsafe {
        pac::NVIC::unmask(pac::Interrupt::IO_IRQ_BANK0);
    }

    loop {
        // interrupts handle everything else in this example.
        cortex_m::asm::wfi();
    }
}

#[allow(non_snake_case)]
#[interrupt]
fn IO_IRQ_BANK0() {
    // The `#[interrupt]` attribute covertly converts this to `&'static mut Option<LedAndRtc>`
    static mut PERIPHS: Option<InitializedPeripherals> = None;

    // This is one-time lazy initialisation. We steal the variables given to us
    // via `GLOBAL_SHARED`.
    if PERIPHS.is_none() {
        critical_section::with(|cs| {
            *PERIPHS = GLOBAL_SHARED.borrow(cs).take();
        });
    }

    // PERIPHS is an `&'static mut Option<LedAndRtc>` thanks to the interrupt macro's magic.
    // The pattern binding mode handles an ergonomic conversion of the match from `if let Some(led_and_rtc)`
    // to `if let Some(ref mut led_and_rtc)`.
    //
    // https://doc.rust-lang.org/reference/patterns.html#binding-modes
    if let Some(periphs) = PERIPHS {
        // borrow led and rtc by *destructuring* the tuple
        // these will be of type `&mut LedPin` and `&mut RealTimeClock`, so we don't have
        // to move them back into the static after we use them
        // let (led, display, rtc, buf) = periphs;
        let (led, in_pin, rosc, rtc, buf) = periphs;

        // let now = rtc.now().unwrap();

        // // Update the OLED

        // // First, format some text into a static buffer:
        // buf.reset();
        // write!(buf, "{:02}:{:02}:{:02}", now.hour, now.minute, now.second).unwrap();

        // // Empty the display
        // display.clear();

        // // Draw 3 lines of text:
        // let text_style = MonoTextStyleBuilder::new()
        //     .font(&FONT_9X18_BOLD)
        //     .text_color(BinaryColor::On)
        //     .build();
        // Text::with_baseline("Hello world!", Point::zero(), text_style, Baseline::Top)
        //     .draw(display)
        //     .unwrap();
        // Text::with_baseline("Hello Rust!", Point::new(0, 16), text_style, Baseline::Top)
        //     .draw(display)
        //     .unwrap();
        // Text::with_baseline(buf.as_str(), Point::new(0, 16 * 2), text_style, Baseline::Top)
        //     .draw(display)
        //     .unwrap();

        // display.flush().unwrap();

        // Toggle the led
        let _ = led.toggle();

        // // Schedule the next alarm
        // rtc.schedule_alarm(rtc::DateTimeFilter::default().second(
        //     (now.second + &INTERVAL_SEC) % 60
        // ));

        rtc.clear_alarm1_matched_flag().unwrap();
        let now = rtc.time().unwrap();
        rtc.set_alarm1_hms(now.add(Duration::from_secs(1))).unwrap();

        // Clear the interrupt flag so it can be triggered again.
        // rtc.clear_interrupt();
        in_pin.clear_interrupt(EdgeLow);

        // Enter DORMANT state
        unsafe {
            rosc.dormant();
        }
    }
}

/// This is a very simple buffer to pre format a short line of text
/// limited arbitrarily to 64 bytes.
struct FmtBuf {
    buf: [u8; 64],
    ptr: usize,
}

impl FmtBuf {
    fn new() -> Self {
        Self {
            buf: [0; 64],
            ptr: 0,
        }
    }

    fn reset(&mut self) {
        self.ptr = 0;
    }

    fn as_str(&self) -> &str {
        core::str::from_utf8(&self.buf[0..self.ptr]).unwrap()
    }
}

impl core::fmt::Write for FmtBuf {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        let rest_len = self.buf.len() - self.ptr;
        let len = if rest_len < s.len() {
            rest_len
        } else {
            s.len()
        };
        self.buf[self.ptr..(self.ptr + len)].copy_from_slice(&s.as_bytes()[0..len]);
        self.ptr += len;
        Ok(())
    }
}

// End of file
